package org.jbooster.enumdocs.generator

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.JavaPlugin

/**
 * @author jearton
 * @since 2017/9/13 上午3:53
 */
class EnumDocsGeneratorPlugin implements Plugin<Project> {

    @Override
    void apply(Project project) {
        project.pluginManager.apply(JavaPlugin)
        project.extensions.create("enumdocs", EnumDocsExtension)
        project.task("enumdocs", type: EnumDocsGeneratorTask).dependsOn('jar')
    }
}
