package org.jbooster.enumdocs.generator

import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.specs.Specs
import org.gradle.api.tasks.TaskAction
import org.jbooster.core.enums.meta.EnumResourceLoader
import org.thymeleaf.TemplateEngine
import org.thymeleaf.context.Context
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver

/**
 * @author jearton
 * @since 2017/9/13 上午3:56
 */
class EnumDocsGeneratorTask extends DefaultTask {

    def LOG_PREFIX = '[EDG]'

    @TaskAction
    void generatorAction() {
        println "${LOG_PREFIX} BEGIN"

        // 读取配置参数
        def packageName = project.enumdocs.packageName as String
        if (!packageName) {
            throw new GradleException("packageName may not be empty")
        }

        def title = project.enumdocs.title as String
        if (!title) {
            throw new GradleException("title may not be empty")
        }

        def outputFile = project.file(project.enumdocs.output as String)
        if (!outputFile.parentFile.exists()) {
            outputFile.parentFile.mkdirs()
        }

        // 构造类加载器
        def jars = project.configurations.runtime.fileCollection(Specs.SATISFIES_ALL)
        jars += project.fileTree(dir: project.rootDir, include: "**/$project.libsDirName/*.jar")
        def urls = jars.files.collect { it.toURI().toURL() }.toArray() as URL[]
        def classLoader = new URLClassLoader(urls, Thread.currentThread().contextClassLoader)

        // 读取枚举数据
        def enumMetas = new TreeSet<>(EnumResourceLoader.findClassPathEnums(packageName, classLoader))

        // 解析模板文件
        try {
            def templateResolver = new ClassLoaderTemplateResolver(classLoader)
            templateResolver.setCharacterEncoding("UTF-8")
            templateResolver.setPrefix('org/jbooster/enumdocs/generator/templates/')
            templateResolver.setSuffix('.html')

            def engine = new TemplateEngine()
            engine.setTemplateResolver(templateResolver)

            def ctx = new Context()
            ctx.setVariable("title", title)
            ctx.setVariable("enumMetas", enumMetas)

            engine.process('enumdocs', ctx, outputFile.newWriter("UTF-8"))
        } finally {
            classLoader.close()
        }

        println "${LOG_PREFIX} END"
    }
}
