# JBooster EnumDocs Gradle Plugin

## Introduction
Generate enum docs with html format.

## Quick Start
- 引入插件
    ```groovy
    plugins {
        id 'org.jbooster.enumdocs.generator' version '0.0.1'
    }
    ```
    
- 参数配置
    ```groovy
    enumdocs {
        // 标题（必须）
        title = 'Demo'
      
        // 在指定包名下扫描所有标准枚举
        packageName = 'x.y.z'
      
        // 运行结果输出到指定文件，一般为html文件
        output = "$projectDir/src/main/resources/static/index.html"
    }
    ```

- 执行task
    ```bash
    gradle enumdocs
    ```
